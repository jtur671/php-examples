<?php
$favcolor = "red";

switch ($favcolor) {
    case "brown":
        echo "Your favorite color is brown!";
        break;
    case "red":
        echo "Your favorite color is red!";
        break;
    case "purple":
        echo "Your favorite color is purple!";
        break;
    default:
        echo "Your favorite color is neither red, blue, or green!";
}
?>